import 'dart:math';

import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

void main() {
  runApp(MyApp());
}

class MyApp extends StatelessWidget {
  // This widget is the root of your application.
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
        home: Scaffold(
          backgroundColor: Colors.lightBlue[300],
          appBar:AppBar(
            backgroundColor: Colors.blue,
            title: Text("Ask Me Anything"),
          ),
          body: AnswerPage(),
        )
    );
  }
}


class AnswerPage extends StatefulWidget {
  //const AnswerPage({Key key}) : super(key: key);

  @override
  _AnswerPageState createState() => _AnswerPageState();
}

class _AnswerPageState extends State<AnswerPage> {
  int ballNumber = 5;

  void changeBallFace(){
    setState(() {
      ballNumber = Random().nextInt(5)+1;
    });
  }

  @override
  Widget build(BuildContext context) {
    return Container(
      child: Center(
          child: Row(
              children: [
                Expanded(
                  child: GestureDetector(
                    onTap: (){
                      changeBallFace();
                    },
                    child: Padding(
                      padding: EdgeInsets.all(16),
                      child: Image(
                        image: AssetImage("assets/images/ball${ballNumber.toString()}.png"),
                      ),
                    ),
                  ),
                ),
              ]
          )
      ),
    );
  }
}


